package Misc;

import Piezas.GTablero;
import Piezas.TableroAmenaces;
import Modelos.LinkedComboBoxModel;
import Piezas.Pieza;
import Piezas.Pieza.tPieza;
import Piezas.PiezaAbstracta;
import Piezas.TableroAmenaces.taIterador;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author tegus
 */
public class nReines11 extends JFrame
{
    static TableroAmenaces t;
    static int n = 8;
    static boolean actiu, solucio;

    
    //Items gràfics
    JPanel     pOpciones;
    JButton    bAñadir;
    JButton    bEliminar;
    JButton    bReset;
    JButton    bIniciar;
    JTextField tfDimensio;
    JComboBox  cbPiezas;
    LinkedComboBoxModel<tPieza> cbmPiezas;
    static GTablero   gt;

    
    //Inicialización del JFrame-------------------------------------------------
    public nReines11()
    {
        super("Colocando piezas");
        this.initContents();
        this.setSize(625, 700);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     * Mètode que inicialitza 
     *  el tauler per defecte
     *  la coa de peces
     *  y tota la part gràfica
     */
    private void initContents()
    {
        t       = new TableroAmenaces(8);
        actiu   = false;
        
        ActionListener botonListener = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                
                JButton b = (JButton) e.getSource();
                              
                if(b == bReset)
                {
                    try
                    {
                        n = Integer.parseInt(tfDimensio.getText());
                        if (n < 1 || n > 41) throw new Exception();
                    } 
                    catch (Exception ex) 
                    {
                        JOptionPane.showMessageDialog(null,"Introdueixi un nombre vàlid");
                        n = 8;
                    }
                    
                    actiu = false;
                    t = new TableroAmenaces(n);
                    gt.changeChessBoard(t);
                    getContentPane().repaint();
                }
            }
        };

        //boton Reset
        bReset = new JButton("Reset");
        bReset.setBounds(270, 7, 80, 22);
        bReset.addActionListener(botonListener);
        
        //campDeText dimensió
        tfDimensio = new JTextField();
        tfDimensio.setBounds(360, 7, 30, 22);
        tfDimensio.setText("8");
        
        //ComboBox
        cbmPiezas = new LinkedComboBoxModel();
        for( tPieza cp : tPieza.values() ) cbmPiezas.addElement(cp);
        cbPiezas = new JComboBox(cbmPiezas);
        cbPiezas.setSelectedIndex(0);
        cbPiezas.setBounds(10, 7, 150, 22);
        
        
        //JPanel opciones
        pOpciones = new JPanel();
        pOpciones.setLayout(null);
        pOpciones.setBounds(10, 10, 600, 30);
        pOpciones.add(bReset);
        pOpciones.add(tfDimensio);
        //pOpciones.add(cbPiezas);

        
        MouseAdapter panellListener = new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                
                final int x = e.getX();
                final int y = e.getY()-20;
                
                final Point inicio = new Point( y / gt.getCostat(), x / gt.getCostat());
                tPieza pi = (tPieza) cbPiezas.getSelectedItem(); //tPieza.REINA
                
                System.out.println(inicio +";");
                
                if(e.getButton() == MouseEvent.BUTTON2 )
                {
                    if ( !gt.addPiezaKilling(tPieza.REINA, inicio) )//(pi, inicio) )
                        JOptionPane.showMessageDialog(null,"No se han podido colocar");
                    else
                    {
                        //if(pi == tPieza.REINA) n--;
                        t = gt.getTauler();
                    }
                }
                
                else
                {
                    if(!actiu)
                    {
                        new Thread() { public void run()
                        {
                            actiu = true;
                            System.out.println("S'ha iniciat la cerca");
                            long tiempo = -System.nanoTime();
                            
                            solucio = colocarPeces(t, inicio, n);
                            
                            if( solucio )
                            {
                                System.out.println(t +"\nColocadas con éxito");
                                gt.changeChessBoard(t);
                                gt.repaint();
                                tiempo += System.nanoTime();
                                JOptionPane.showMessageDialog(null,"Colocadas en "+ (double)(tiempo)/1000000000 +" segundos");
                            }

                            else
                            { 
                                System.out.println("no se han podido colocar");
                                JOptionPane.showMessageDialog(null,"No se han podido colocar");
                            }
                            
                        }}.start();
                        
                        actiu = false;
                    }
                    else { actiu = false; }
                }
            }
        };
        
        //JPanel GTablero
        gt = new GTablero(new TableroAmenaces(8), 600);
        gt.setLayout(null);
        gt.setBounds(10, 40, 605, 640);
        gt.addMouseListener(panellListener);
        
        this.getContentPane().setLayout(null);
        this.getContentPane().add(pOpciones);
        this.getContentPane().add(gt);
    }

    
    //algorisme backtracking----------------------------------------------------
    /**
     * 
     * @param tablero
     * @param pos0
     * @param restants
     * @return 
     */
    public static boolean colocarPeces(TableroAmenaces tablero, Point pos0, int restants) //List<tPieza> colocar)
    {
        boolean encertat = false;
        
        taIterador it = tablero.new taIterador(pos0);
        Point pos = tablero.getRowEmpty(it);
        while( it.is_valid() && tablero.hayPieza(pos) && restants != 0 ) {
            pos = segPosicio(pos);
            it = tablero.new taIterador(pos);
            restants = restants-1;
            pos = tablero.getRowEmpty(it);
        }
        if(restants == 0) {
            t = tablero;
            return true;
        }
        while ( it.is_valid() && !encertat && actiu ) {
            Pieza p = PiezaAbstracta.getPieza(tPieza.REINA);
            if( p.matar(pos, tablero).isEmpty() ) {
                encertat = colocarPeces(
                        tablero.addPiezaKilling(tPieza.REINA, pos),
                        segPosicio(pos),
                        restants-1 //remove*
                );
            }
            pos = tablero.getRowEmpty(it); //tablero.getNextEmpty();
        }
        return encertat && actiu; 
    }
    
    
    //Funcions d'ajuda----------------------------------------------------------
    //public static boolean es_valid(Point p, Point q) { return p != null || p.distance(q) < 1; }

    private static Point segPosicio(Point pos) { 
        return new Point( 
            (int)(pos.getX()+1) % t.getDimension(),
            (int) pos.getY()
        );
    }
    
    public static int pideNumero(String s)
    {
        int entero = 0;
        try 
        { entero = Integer.parseInt(JOptionPane.showInputDialog(s)); }
        catch (Exception e)  { pideNumero(s); }
        return entero;
    }

    //Mètode que inicialitza el JFrame------------------------------------------
    public static void main(String[] args)
    {    
        nReines11 bt = new nReines11();
        bt.setVisible(true);  
    }
    /**/
}