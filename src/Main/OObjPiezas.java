package Main;

import Piezas.GTablero;
import Piezas.TableroAmenaces;
import Modelos.LinkedComboBoxModel;
import Piezas.Pieza;
import Piezas.Pieza.tPieza;
import Piezas.PiezaAbstracta;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author tegus
 */
public class OObjPiezas extends JFrame {
    
    static TableroAmenaces t;
    static List<tPieza> colocar;
    static int n;
    static boolean actiu, solucio;
    
    //Items gràfics
    JPanel     pOpciones;
    JButton    bAñadir;
    JButton    bEliminar;
    JButton    bReset;
    JButton    bIniciar;
    JTextField tfDimensio;
    JLabel     lPiezas;
    JLabel     lInfo;
    JComboBox  cbPiezas;
    LinkedComboBoxModel<tPieza> cbmPiezas;
    GTablero   gt;

    
    //Inicialización del JFrame-------------------------------------------------
    public OObjPiezas()
    {
        super("Colocando piezas");
        this.initContents();
        this.setSize(350, 570);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     * Mètode que inicialitza 
     *  el tauler per defecte
     *  la coa de peces
     *  y tota la part gràfica
     */
    private void initContents()
    {
        colocar = new LinkedList<>();
        t       = new TableroAmenaces(8);
        actiu   = false;
        
        ActionListener botonListener = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                
                JButton b = (JButton) e.getSource();
                
                if(b == bAñadir)
                {
                    tPieza p = (tPieza) cbPiezas.getSelectedItem();
                    colocar.add(p);
                    lPiezas.setText("<html>"+ colocar.toString() +"</html>");
                }
                
                else if(b == bEliminar)
                {
                    final int s = colocar.size();
                    if( s > 0 )
                    {
                        colocar.remove(colocar.size()-1);
                        if(s == 1) lPiezas.setText("Lista vacía");
                        else lPiezas.setText("<html>"+ colocar.toString() +"</html>");
                    }

                }
                
                else if(b == bReset)
                {
                    try
                    {
                        n = Integer.parseInt(tfDimensio.getText());
                        if (n < 1 || n > 25) throw new Exception();
                    } 
                    catch (Exception ex) 
                    {
                        JOptionPane.showMessageDialog(null,"Introdueixi un nombre vàlid");
                        n = 8;
                    }
                    actiu = false;
                    t = new TableroAmenaces(n);
                    gt.changeChessBoard(t);
                    getContentPane().repaint();
                    colocar.clear();
                    lPiezas.setText("Lista vacía");
                }
                
                else if( b == bIniciar)
                {
                    if(!actiu)
                    {
                        System.out.println("S'ha iniciat la cerca");
                        new Thread() { public void run()
                        {
                            actiu = true;
                            solucio = colocarPeces(t, colocar);
                            
                            if( solucio )
                            {
                                System.out.println(t +"\nColocadas con éxito");
                                gt.changeChessBoard(t);
                                gt.repaint();
                            }

                            else
                            { JOptionPane.showMessageDialog(null,"No se han podido colocar"); }
                            
                            colocar.clear();
                            lPiezas.setText("Lista vacía");
                        }}.start();
                        
                        actiu = false;
                    }
                    else { actiu = false; }
                }
            }
        };
        
        //boton Añadir Pieza
        bAñadir = new JButton("Añadir");
        bAñadir.setBounds(170, 10, 138, 22);
        bAñadir.addActionListener(botonListener);
        
        //boton Eliminar Pieza
        bEliminar = new JButton("Eliminar última");
        bEliminar.setBounds(170, 40, 138, 22);
        bEliminar.addActionListener(botonListener);
        
        //boton Reset
        bReset = new JButton("Reset");
        bReset.setBounds(170, 70, 80, 22);
        bReset.addActionListener(botonListener);
        
        //campDeText dimensió
        tfDimensio = new JTextField();
        tfDimensio.setBounds(260, 70, 30, 22);
        tfDimensio.setText("8");
        
        //boton Iniciar
        bIniciar = new JButton("Iniciar");
        bIniciar.setBounds(170, 100, 80, 22);
        bIniciar.addActionListener(botonListener);
        
        //ComboBox
        cbmPiezas = new LinkedComboBoxModel();
        for( tPieza cp : tPieza.values() ) cbmPiezas.addElement(cp);
        cbPiezas = new JComboBox(cbmPiezas);
        cbPiezas.setSelectedIndex(0);
        cbPiezas.setBounds(10, 10, 150, 22);
        
        //JLabel Piezas
        lPiezas = new JLabel("Lista vacía");
        lPiezas.setBounds( 10, 25, 150, 50);
        
        //JLabel Info ------------------------------------------------------
        lInfo = new JLabel("<html>Haz click en alguna casilla para añadir piezas</html>");
        lInfo.setBounds( 10, 70, 150, 50);
        
        //JPanel opciones
        pOpciones = new JPanel();
        pOpciones.setLayout(null);
        pOpciones.setBounds(10, 10, 600, 150);
        pOpciones.add(bAñadir);
        pOpciones.add(bEliminar);
        pOpciones.add(bReset);
        pOpciones.add(bIniciar);
        pOpciones.add(tfDimensio);
        pOpciones.add(cbPiezas);
        pOpciones.add(lPiezas);
        pOpciones.add(lInfo);
        
        MouseAdapter panellListener = new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                
                final int x = e.getX();
                final int y = e.getY()-10;
                
                Point po = new Point( y / gt.getCostat(), x / gt.getCostat());
                tPieza pi = (tPieza) cbPiezas.getSelectedItem();
                
                System.out.println(po +"; "+ pi);
                
                if ( !gt.addPiezaKilling(pi, po) )
                    JOptionPane.showMessageDialog(null,"No se han podido colocar");
                else
                    t = gt.getTauler();
            }
        };
        
        //JPanel GTablero
        gt = new GTablero(new TableroAmenaces(8), 320);
        gt.setLayout(null);
        gt.setBounds(10, 180, 600, 600);
        gt.addMouseListener(panellListener);
        
        this.getContentPane().setLayout(null);
        this.getContentPane().add(pOpciones);
        this.getContentPane().add(gt);
    }

    
    //algorisme backtracking----------------------------------------------------
    /**
     * 
     * @param tablero
     * @param colocar
     * @return 
     */
    public static boolean colocarPeces(TableroAmenaces tablero, List<tPieza> colocar)
    {
        boolean encertat = colocar.isEmpty();
        if( encertat ) t = tablero;
        else
        {
            tPieza Pieza0 = colocar.remove(0);
            Point pos = tablero.getFirstEmpty();
            while ( es_valid(pos) && !encertat && actiu )
            {
                Pieza p = PiezaAbstracta.getPieza(Pieza0);
                if( p.matar(pos, tablero).isEmpty() )
                {
                    encertat = colocarPeces(
                            tablero.addPiezaKilling(Pieza0, pos),
                            new LinkedList<>(colocar)
                    );
                }
                pos = tablero.getNextEmpty(pos);
            }
        }
        return encertat && actiu;
    }
    
    
    //Funcions d'ajuda----------------------------------------------------------
    public static boolean es_valid(Point p) { return p != null; }

    //Mètode que inicialitza el JFrame------------------------------------------
    public static void main(String[] args)
    {    
        OObjPiezas bt = new OObjPiezas();
        bt.setVisible(true);
    }
    /**/
}