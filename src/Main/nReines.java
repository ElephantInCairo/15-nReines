package Main;

import Piezas.GTablero;
import Piezas.TableroAmenaces;
import Piezas.Pieza;
import Piezas.Pieza.tPieza;
import Piezas.PiezaAbstracta;
import Piezas.TableroAmenaces.taIterador;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author tegus
 */
public class nReines extends JFrame
{
    static TableroAmenaces tauler;
    static int n = 8;
    static boolean actiu, solucio;
    
    //Items gràfics
    JPanel     pOpciones;
    JButton    bReset;
    JTextField tfDimensio;
    GTablero   gt;

    
    //Inicialización del JFrame-------------------------------------------------
    public nReines()
    {
        super("Colocando piezas");
        this.initContents();
        this.setSize(625, 700);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     * Mètode que inicialitza 
     *  el tauler per defecte
     *  la coa de peces
     *  y tota la part gràfica
     */
    private void initContents()
    {
        tauler  = new TableroAmenaces(8);
        actiu   = false;
        
        ActionListener botonListener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                JButton b = (JButton) e.getSource();
                
                if(b == bReset)
                {
                    try {
                        n = Integer.parseInt(tfDimensio.getText());
                        if (n < 1 || n > 41) throw new Exception();
                    }
                    catch (Exception ex) {
                        JOptionPane.showMessageDialog(null,"Introdueixi un nombre vàlid");
                        n = 8;
                    }
                    
                    actiu = false;
                    tauler = new TableroAmenaces(n);
                    gt.changeChessBoard(tauler);
                    getContentPane().repaint();
                }
            }
        };

        //boton Reset
        bReset = new JButton("Reset");
        bReset.setBounds(270, 7, 80, 22);
        bReset.addActionListener(botonListener);
        
        //campDeText dimensió
        tfDimensio = new JTextField();
        tfDimensio.setBounds(360, 7, 30, 22);
        tfDimensio.setText("8");
        
        //ComboBox
//        cbmPiezas = new LinkedComboBoxModel();
//        for( tPieza cp : tPieza.values() ) cbmPiezas.addElement(cp);
//        cbPiezas = new JComboBox(cbmPiezas);
//        cbPiezas.setSelectedIndex(0);
//        cbPiezas.setBounds(10, 7, 150, 22);
        
        
        //JPanel opciones
        pOpciones = new JPanel();
        pOpciones.setLayout(null);
        pOpciones.setBounds(10, 10, 600, 30);
        pOpciones.add(bReset);
        pOpciones.add(tfDimensio);
        //pOpciones.add(cbPiezas);


        MouseAdapter panellListener = new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                final int x = e.getX();
                final int y = e.getY()-20;
                
                final Point inicio = new Point( y / gt.getCostat(), x / gt.getCostat());
                //tPieza pi = (tPieza) cbPiezas.getSelectedItem(); //tPieza.REINA
                
                System.out.println(inicio +";");
                
                if(e.getButton() == MouseEvent.BUTTON3 ) {
                    
                    if ( !gt.addPiezaKilling(tPieza.REINA, inicio) )//(pi, inicio) )
                        JOptionPane.showMessageDialog(null,"No se han podido colocar");
                    
                    else tauler = gt.getTauler(); 
                }
                
                else
                {
                    if(!actiu)
                    {
                        new Thread() {@Override public void run()
                        {
                            System.out.println("S'ha iniciat la cerca");
                            actiu = true;
                            long tiempo = -System.nanoTime();
                            
                            solucio = colocarPeces(tauler, inicio, n);
                            
                            tiempo += System.nanoTime();
                            actiu = false;
                            
                            if( solucio ) {
                                System.out.println(tauler +"\nColocadas con éxito");
                                gt.changeChessBoard(tauler);
                                gt.repaint();
                                JOptionPane.showMessageDialog(null,"Colocadas en "+ (double)(tiempo)/1000000000 +" segundos");
                            }

                            else { 
                                System.out.println("no se han podido colocar");
                                JOptionPane.showMessageDialog(null,"No se han podido colocar\n"
                                        +"tiempo trancurrido: "+ (double)(tiempo)/1000000000 +" segundos");
                            }
                            
                        }}.start();
                    }
                    
                    else actiu = false; 
                }
            }
        };
        
        //JPanel GTablero
        gt = new GTablero(new TableroAmenaces(8), 600);
        gt.setLayout(null);
        gt.setBounds(10, 40, 605, 640);
        gt.addMouseListener(panellListener);
        
        this.getContentPane().setLayout(null);
        this.getContentPane().add(pOpciones);
        this.getContentPane().add(gt);
    }

    
    //algorisme backtracking----------------------------------------------------
    /**
     * 
     * @param tablero
     * @param pos0
     * @param restants
     * @return 
     */
    public static boolean colocarPeces(TableroAmenaces tablero, Point pos0, int restants)
    {
        boolean encertat = restants == 0;
        if(encertat) tauler = tablero;
        else {
            taIterador it = tablero.new taIterador(pos0);
            Point pos = tablero.getRowEmpty(it);
            while ( it.is_valid() && (!encertat && actiu || tablero.hayPieza(pos)) )
            {
                Pieza p = PiezaAbstracta.getPieza(tPieza.REINA);
                if( p.matar(pos, tablero).isEmpty() || tablero.hayPieza(pos) ) {
                    encertat = colocarPeces(
                            tablero.addPiezaKilling(tPieza.REINA, pos),
                            segPosicio(pos),
                            restants-1
                    );
                }
                pos = tablero.getRowEmpty(it);
            }
        }
        return encertat && actiu; 
    }
    
    
    //Funcions d'ajuda----------------------------------------------------------
    private static Point segPosicio(Point pos)
    {
        return new Point( 
            (int)(pos.getX()+1) % tauler.getDimension(),
            (int) pos.getY()
        );
    }

    //Mètode que inicialitza el JFrame------------------------------------------
    public static void main(String[] args)
    {
        nReines bt = new nReines();
        bt.setVisible(true);  
    }
    /**/
}