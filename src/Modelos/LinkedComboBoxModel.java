/** José Sánchez y Javier Aylman, Algoritmia.
 */
package Modelos;

import java.util.Comparator;
import javax.swing.ComboBoxModel;

/**
 *
 * @author tegus
 * @param <E>
 */
public class LinkedComboBoxModel<E> 
        extends AbstractLinkedModel<E>
        implements ComboBoxModel<E> {
    
    E elementoSeleccionado;
    
//    public <E> LinkedComboBoxModel(Comparator<E> cmp) {
//        super(cmp);
//        
//        if(!lista.isEmpty())
//            elementoSeleccionado = lista.iterator().next();//getPrimero().getObject();
//    }
    
    public <E> LinkedComboBoxModel() {
        super();
        
        if(!lista.isEmpty())
            elementoSeleccionado = lista.iterator().next();//getPrimero().getObject();
    }
    
    @Override
    public void setSelectedItem(Object anItem) {
        elementoSeleccionado = (E) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return elementoSeleccionado;
    }
    
    @Override
    public void addElement(E e) {
        if (lista.add(e)) {
            int pos = lista.indexOf(e);
            fireIntervalAdded(this, pos, pos);
        }
    }    
}
