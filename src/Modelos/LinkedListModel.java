/** José Sánchez y Javier Aylman, Algoritmia.
 */
package Modelos;

import java.util.Comparator;

/**
 *
 * @author tegus
 * @param <E>
 */
public class LinkedListModel<E> extends AbstractLinkedModel<E> {

    public <E> LinkedListModel(Comparator<E> cmp) {
        super(/*cmp*/);
    }
    
    @Override
    public void addElement(E e) {
        if (lista.add(e)) {
            int pos = lista.indexOf(e);
            fireIntervalAdded(this, pos, pos);
        }
    }
    /**/
}
