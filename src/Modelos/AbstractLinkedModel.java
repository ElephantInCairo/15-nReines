package Modelos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author tegus
 * @see ListModel
 * @param <E>
 */
public abstract class AbstractLinkedModel<E>
        extends AbstractListModel<E> {
    
    List<E> lista;

//    public <E> AbstractLinkedModel(Comparator<E> cmp) {
//        this.lista = ListFactory.getLista(cmp);
//    }
    
    public <E> AbstractLinkedModel() {
        this.lista = new ArrayList();
    }
    
    @Override
    public int getSize() {
        return lista.size();
    }

    @Override
    public E getElementAt(int index) {
        return lista.get(index);
    }

    abstract public void addElement(E e);
    
    public boolean removeElement(E e) {
        
        int pos = lista.indexOf(e);
        
        if(pos >= 0)
            fireIntervalRemoved(this, pos, pos);
        
        return lista.remove(e);
    }
    
    public int getPosition(E e) {
        return lista.indexOf(e);
    }
    
    private void addAll(Collection<E> lista) {
        this.lista.addAll(lista);
    }
    
    private void clear() {
        int size = getSize();
        lista.clear();
        this.fireIntervalRemoved(this, 0, size);
    }
    
    public void rebuild(Collection<? extends E> lista) {
        clear();
        addAll((Collection<E>) lista);
    }
    
    public void rebuild(E e) {
        clear();
        addElement(e);
    }
    /**/
}
