/**
 * Si se añade un campo para tablero, no hace falta enviarselo en cada llamada
 *  a mover o matar, la pieza queda ligada a un tablero.
 */
package Piezas;

import Piezas.Logic.Alfil;
import Piezas.Logic.Cavall;
import Piezas.Logic.Peon;
import Piezas.Logic.Reina;
import Piezas.Logic.Rey;
import Piezas.Logic.Rey2;
import Piezas.Logic.Torre;

/**
 *
 * @author tegus
 */
abstract public class PiezaAbstracta implements Pieza {
    
    private final boolean color;
    
    public PiezaAbstracta(boolean color) { this.color = color; }
    
    /**
     * Por compatibilidad con "más equipos" debería retornar el equipo al que
     * pertenece. Además, si no és necesario saber a que equipo pertenece y
     * es suficiente saber si otra pieza es del mismo equipo o no, este método
     * podria ser privado.
     * 
     * @return 
     */
    @Override
    public boolean esBlanca() { return color; }
    
    @Override
    public boolean esMismoColor(Pieza p) {
        return false;
        //return p.esBlanca() == esBlanca();
    }
    
    //SINGLETON-----------------------------------------------------------------
    
    //Només es guarda una sola instància en memòria



    



    
    public static Pieza getPieza(tPieza cPieza)
    {
        switch ( cPieza )
        {
            case REINA:  return Reina.getInstance();
            case TORRE:  return Torre.getInstance();
            case ALFIL:  return Alfil.getInstance();
            case CAVALL: return Cavall.getInstance();
            case REY:    return Rey.getInstance();
            case REY2:   return Rey2.getInstance();
            case PEO:    return Peon.getInstance();
            default:     return Reina.getInstance();
        }
    }
}
