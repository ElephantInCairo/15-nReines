package Piezas;

import Piezas.Pieza.tPieza;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author tegus
 */
public class GTablero extends JPanel {
    
    TableroAmenaces t;
    private final int tamany;
    private int COSTAT;
    
    public GTablero(TableroAmenaces t, int tamany)
    {
        this.t = t;
        this.tamany = tamany;
        this.COSTAT = (tamany/t.getDimension());
    }
    
    public int getCostat() { return COSTAT; }
    
    public TableroAmenaces getTauler() { return t; }
    
    public void changeChessBoard (TableroAmenaces nt)
    {
        t = nt;
        this.COSTAT = (tamany/t.getDimension());
        this.repaint();
    }
    
    public boolean addPiezaKilling(tPieza pieza, Point pos)
    {
        if (!t.hayPieza(pos) && !t.isAmenaçada(pos))
        {
            changeChessBoard(t.addPiezaKilling(pieza, pos));
            return true;
        }
        
        else return false;   
    }
    
    @Override
    public Dimension getPreferredSize() { return new Dimension(t.getDimension()*COSTAT+1, t.getDimension()*COSTAT+26); }
    
    
    @Override
    public void paintComponent(Graphics g)
    {
        Graphics2D g2d = (Graphics2D) g;
        int max = t.getDimension();
        
        //pintar casillas
        for (int i = 0; i < max; i++)
        {
            for (int j = 0; j < max; j++)
            {
                Rectangle2D.Float rec = new Rectangle2D.Float(i*COSTAT, j*COSTAT+20, COSTAT, COSTAT);
                
                if((i+j)%2 == 0) 
                {
                    //relleno
                    if(t.isAmenaçada(new Point(j, i))) g2d.setColor(Color.RED);
                    else                               g2d.setColor(Color.BLACK);
                    g2d.fill(rec);
                    
                    //contorn
                    g2d.setColor(Color.BLACK);
                    g2d.draw(rec);
                }
                
                else 
                {
                    //relleno
                    if(t.isAmenaçada(new Point(j, i))) g2d.setColor(Color.ORANGE);
                    else                               g2d.setColor(Color.LIGHT_GRAY);
                    g2d.fill(rec);
                    
                    //contorn
                    g2d.setColor(Color.BLACK);
                    g2d.draw(rec);
                }
            }
        }
        
        //pintar piezas
        for (int i = 0; i < max; i++)
        {
            for (int j = 0; j < max; j++)
            {
                tPieza p = t.getPieza(new Point(i, j));
                if( p != null )
                {
                    BufferedImage img;
                    try {
                        img = ImageIO.read(new File("peces/"+ p +".png"));
                        g.drawImage(img,(int) j*COSTAT, (int) i*COSTAT+10, COSTAT, COSTAT, null);
                    } 
                    catch (IOException e) {}
                }   
            }
        }
    }
    /**/
}
