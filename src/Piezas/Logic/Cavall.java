package Piezas.Logic;

import Piezas.Pieza;
import Piezas.TableroAmenaces;
import Piezas.Pieza.Direccion;
import Piezas.PiezaAbstracta;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tegus
 */
public class Cavall extends PiezaAbstracta {

    private static final Pieza c = new Cavall(true);
    private final List<Direccion> direccionesH;
    private final List<Direccion> direccionesV;
    
    private Cavall(boolean color) {
        super(color);
        direccionesH = new ArrayList();
        direccionesV = new ArrayList();
        direccionesH.add(Direccion.E);
        direccionesH.add(Direccion.O);
        direccionesV.add(Direccion.N);
        direccionesV.add(Direccion.S);
    }

    @Override
    public List amenazar(Point p, TableroAmenaces t) {
        
        List<Point> amenazar = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direccionesH) {
            for(Direccion d2 : direccionesV) {
                
                p.setLocation(inicio);
                //se mueve de su posicion inicial
                d.avança(p);
                d2.avança(p);
                d2.avança(p);
                
                if( t.dentroTablero(p) ) 
                    amenazar.add(new Point(p));
                
            }
        }
        
        for(Direccion d : direccionesV) {
            for(Direccion d2 : direccionesH) {
                
                p.setLocation(inicio);
                //se mueve de su posicion inicial
                d.avança(p);
                d2.avança(p);
                d2.avança(p);
                
                if( t.dentroTablero(p) ) 
                    amenazar.add(new Point(p));
            }
        }
        p.setLocation(inicio);
    
        return amenazar;
    }

    @Override
    public List matar(Point p, TableroAmenaces t) {
        
        List<Point> matar = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direccionesH) {
            for(Direccion d2 : direccionesV) {
                
                p.setLocation(inicio);
                //se mueve de su posicion inicial
                d.avança(p);
                d2.avança(p);
                d2.avança(p);
                
                if( t.dentroTablero(p) && t.hayPieza(p) ) 
                    matar.add(new Point(p));
                
            }
        }
        
        for(Direccion d : direccionesV) {
            for(Direccion d2 : direccionesH) {
                
                p.setLocation(inicio);
                //se mueve de su posicion inicial
                d.avança(p);
                d2.avança(p);
                d2.avança(p);
                
                if( t.dentroTablero(p) && t.hayPieza(p) ) 
                    matar.add(new Point(p));
            }
        }
        p.setLocation(inicio);
        
        return matar;
    }

    @Override
    public List mover(Point p, TableroAmenaces t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() { return "C"; }
    
    public static Pieza getInstance() { return c; }
}
