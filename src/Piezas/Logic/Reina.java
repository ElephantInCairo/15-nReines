package Piezas.Logic;

import Piezas.Pieza;
import Piezas.Pieza.Direccion;
import Piezas.PiezaLineal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tegus
 */
public class Reina extends PiezaLineal {

    private static final Pieza q = new Reina(true);
    
    List<Direccion> direcciones;

    private Reina(boolean color) {
        super(color);
        direcciones = new ArrayList<>();
        direcciones.add(Direccion.N);
        direcciones.add(Direccion.NE);
        direcciones.add(Direccion.NO);
        direcciones.add(Direccion.O);
        direcciones.add(Direccion.E);
        direcciones.add(Direccion.S);
        direcciones.add(Direccion.SO);
        direcciones.add(Direccion.SE);
    }

    @Override
    public List getDirecciones() { return direcciones; }
    
    @Override
    public String toString() { return "Q"; }
    
    public static Pieza getInstance() { return q; }
    /**/
}
