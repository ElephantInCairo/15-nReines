package Piezas.Logic;

import Piezas.PiezaAbstracta;
import Piezas.TableroAmenaces;
import Piezas.Pieza;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tegus
 */
public class Rey extends PiezaAbstracta {
    
    private static final Pieza r = new Rey(true);
    private final List<Direccion> direcciones;

    private Rey(boolean color) {
        super(color);
        direcciones = new ArrayList();
        direcciones.add(Direccion.N);
        direcciones.add(Direccion.S);
        direcciones.add(Direccion.E);
        direcciones.add(Direccion.O);
        direcciones.add(Direccion.NE);
        direcciones.add(Direccion.SO);
        direcciones.add(Direccion.NO);
        direcciones.add(Direccion.SE);
    }

    @Override
    public List matar(Point p, TableroAmenaces t) {
        
        List<Point> matar = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direcciones) {
            //se mueve de su posicion inicial
            d.avança(p);
            //mientras la casilla esté dentro y esté llena, haz
            if( t.dentroTablero(p) && t.hayPieza(p) ) {
                //si es de diferente color, la añade
                //if( esMismoColor( t.getPieza(p) ) ) 
                    matar.add(new Point(p));
            }
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        p.setLocation(inicio);
        return matar;
    }

    @Override
    public List mover(Point p, TableroAmenaces t) {
        
        List<Point> mover = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direcciones) {
            //se mueve de su posicion inicial
            d.avança(p);
            //si la casilla es valida Y está vacia, añade
            if( t.dentroTablero(p) && !t.hayPieza(p) ) 
                mover.add(new Point(p)); 
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        
        return mover;
    }

    @Override
    public List amenazar(Point p, TableroAmenaces t) {
        
        List<Point> amenazar = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direcciones) {
            //se mueve de su posicion inicial
            d.avança(p);
            //si la casilla es valida Y está vacia, añade
            if( t.dentroTablero(p) ) 
                amenazar.add(new Point(p)); 
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        
        return amenazar;
    }
    
    @Override
    public String toString() { return "R"; }
    
    public static Pieza getInstance() { return r; }
}
