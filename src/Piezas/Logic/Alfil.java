package Piezas.Logic;

import Piezas.Pieza;
import Piezas.PiezaLineal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tegus
 */
public class Alfil extends PiezaLineal {
    
    private static final Pieza a = new Alfil(true);
    List<Direccion> direcciones;

    private Alfil(boolean color)
    {
        super(color);
        direcciones = new ArrayList<>();
        direcciones.add(Direccion.NE);
        direcciones.add(Direccion.SO);
        direcciones.add(Direccion.NO);
        direcciones.add(Direccion.SE);
    }

    @Override
    public List getDirecciones() { return direcciones; }
    
    @Override
    public String toString() { return "A"; }
    /**/

    public static Pieza getInstance() { return a; }
}
