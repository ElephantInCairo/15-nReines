package Piezas.Logic;

import Piezas.PiezaAbstracta;
import Piezas.TableroAmenaces;
import Piezas.Pieza;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tegus
 */
public class Rey2  extends PiezaAbstracta {
    
    private static final Pieza k = new Rey2(true);
    private final List<Direccion> direccionesD;
    private final List<Direccion> direccionesR;
    
    private Rey2(boolean color) {
        super(color);
        direccionesR = new ArrayList();
        direccionesD = new ArrayList();
        direccionesR.add(Direccion.N);
        direccionesR.add(Direccion.S);
        direccionesR.add(Direccion.E);
        direccionesR.add(Direccion.O);
        direccionesD.add(Direccion.NE);
        direccionesD.add(Direccion.SO);
        direccionesD.add(Direccion.NO);
        direccionesD.add(Direccion.SE);
    }

    @Override
    public List amenazar(Point p, TableroAmenaces t) {
        
        List<Point> amenazar = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direccionesR) {
            //se mueve de su posicion inicial
            d.avança(p);
            //si la casilla es valida Y está vacia, añade
            if( t.dentroTablero(p) ) 
                amenazar.add(new Point(p)); 
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        
        for(Direccion d : direccionesD) {
            //se mueve de su posicion inicial
            for (int i = 0; i < 2; i++) {
                d.avança(p);
                //si la casilla es valida Y está vacia, añade
                if( t.dentroTablero(p) )
                    amenazar.add(new Point(p));
            }
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        p.setLocation(inicio);
        return amenazar;
    }

    @Override
    public List matar(Point p, TableroAmenaces t) {
        
        List<Point> amenazar = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direccionesR) {
            //se mueve de su posicion inicial
            d.avança(p);
            //si la casilla es valida Y está vacia, añade
            if( t.dentroTablero(p) && t.hayPieza(p) ) 
                amenazar.add(new Point(p)); 
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        
        for(Direccion d : direccionesD) {
            //se mueve de su posicion inicial
            for (int i = 0; i < 2; i++) {
                d.avança(p);
                //si la casilla es valida Y está vacia, añade
                if( t.dentroTablero(p) && t.hayPieza(p) )
                    amenazar.add(new Point(p));
            }
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        p.setLocation(inicio);
        return amenazar;
    }

    @Override
    public List mover(Point p, TableroAmenaces t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() { return "K"; }
    
    public static Pieza getInstance() { return k; }
}
