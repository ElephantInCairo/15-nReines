package Piezas.Logic;

import Piezas.Pieza;
import Piezas.Pieza.Direccion;
import Piezas.PiezaLineal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tegus
 */
public class Torre extends PiezaLineal {
    
    private static final Pieza t = new Torre(true);
    List<Direccion> direcciones;

    private Torre(boolean color) {
        super(color);
        direcciones = new ArrayList<>();
        direcciones.add(Direccion.N);
        direcciones.add(Direccion.S);
        direcciones.add(Direccion.E);
        direcciones.add(Direccion.O);
    }

    @Override
    public List getDirecciones() { return direcciones; }
    
    @Override
    public String toString() { return "T"; }
    /**/
    
    public static Pieza getInstance() { return t; }
}
