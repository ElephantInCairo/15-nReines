package Piezas.Logic;

import Piezas.PiezaAbstracta;
import Piezas.TableroAmenaces;
import Piezas.Pieza;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tegus
 */
public class Peon extends PiezaAbstracta {
    
    private static final Pieza p = new Peon(true);
    private final List<Direccion> direcciones;
    
    //se ha movido toca anar aqui??
    // si, perque influira en es prox. moviments
    private boolean seHaMovido;


    private Peon(boolean color) {
        super(color);
        direcciones = new ArrayList();
        seHaMovido = false;
        
        if (color) {
            direcciones.add(Direccion.NE);
            direcciones.add(Direccion.NO);
        } else {
            direcciones.add(Direccion.SE);
            direcciones.add(Direccion.SO);
        }
    }

    @Override
    public List matar(Point p, TableroAmenaces t) {
        
        List<Point> matar = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direcciones) {
            //se mueve de su posicion inicial
            for (int i = 0; i < 2; i++) {
                d.avança(p);
                //mientras la casilla esté dentro y esté llena, haz
                if( t.dentroTablero(p) && t.hayPieza(p) ) {
                    //si es de diferente color, la añade
                    matar.add(new Point(p));
                }
            }
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        
        return matar;
    }

    @Override
    public List mover(Point p, TableroAmenaces t) {
        
        List<Point> mover = new ArrayList<>();
        Point inicio = new Point(p);
        Direccion d = Direccion.S;
        
        if( esBlanca() )
            d = Direccion.N;
        
        //se mueve de su posicion inicial
        d.avança(p);
        //si la casilla es valida Y está vacia, añade
        if( t.dentroTablero(p) && !t.hayPieza(p) ) {
            mover.add(new Point(p)); 
            d.avança(p);
            if( !seHaMovido && t.dentroTablero(p) && !t.hayPieza(p) ) 
                mover.add(new Point(p));
        }
        //la vuelve a colocar a sitio para que se mueva en otra direccion
        p.setLocation(inicio);
        
        return mover;
    }

    @Override
    public List amenazar(Point p, TableroAmenaces t) {
        
        List<Point> amenazar = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direcciones) {
            //se mueve de su posicion inicial
            for (int i = 0; i < 2; i++) {
                d.avança(p);
                //mientras la casilla esté dentro y esté llena, haz
                if( t.dentroTablero(p) ) {
                    //si es de diferente color, la añade
                    amenazar.add(new Point(p));
                }
            }
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        p.setLocation(inicio);
        
        return amenazar;
    }
    
    @Override
    public String toString() { return "P"; }
    
    public static Pieza getInstance() { return p; }
}
