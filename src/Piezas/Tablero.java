package Piezas;

import Piezas.Pieza.tPieza;
import java.awt.Point;

/**
 *
 * @author tegus
 */
public class Tablero {

    private final int dimension;
    private final tPieza[][] piezas;

    public Tablero(int dimension) {
        this.dimension = dimension;
        this.piezas = new tPieza[dimension][dimension];
    }

    public Tablero(Tablero t) {
        this(t.getDimension());

        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                piezas[i][j] = t.piezas[i][j];
            }
        }
    }

    public int getDimension() {
        return dimension;
    }

    /**
     * Coloca la pieza indicada (pieza) en la pos "p". Si la posición está vacía
     * y la pieza todavía no está en el tablero.
     *
     * @param pieza
     * @param p
     * @return
     */
    public boolean addPieza(tPieza pieza, Point p) {
        if (!(dentroTablero(p) && !hayPieza(p))) {
            return false;
        }

        int x = (int) p.getX();
        int y = (int) p.getY();
        piezas[x][y] = pieza;
        return true;
    }

    /**
     * Devuelve la pieza que esta en la posición indicada o null.
     *
     * @param p
     * @return
     */
    public tPieza getPieza(Point p) {
        if (!dentroTablero(p)) {
            System.out.println("La posición " + p + " no está en el tablero");
            return null;
        }

        int x = (int) p.getX();
        int y = (int) p.getY();
        return this.piezas[x][y];

    }

    /**
     * POLICY. (Behavior) Esta Colección tiene cero o una Pieza en una posición.
     *
     * @param p
     * @return
     */
    public boolean hayPieza(Point p) {
        return null != getPieza(p);
    }

    /**
     * POLICY. (Behavior) Cero incluido, dimension no incluido
     *
     * @param p
     * @return
     */
    public boolean dentroTablero(Point p) {
        int x = (int) p.getX();
        int y = (int) p.getY();
        return (x >= 0) && (y >= 0) && (x < dimension) && (y < dimension);
    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                if (hayPieza(i, j)) {
                    s += " " + getPieza(i, j).representacio();
                } else if ((i + j) % 2 == 0) {
                    s += " ·";
                } else {
                    s += "  ";
                }
            }
            s += "\n";
        }
        return s;
    }

    //No tenen seguretat per ser executats extenament
    /**
     * No es segur
     *
     * @param x
     * @param y
     * @return
     */
    protected boolean hayPieza(int x, int y) {
        return null != piezas[x][y];
    }

    /**
     * Devuelve la pieza que esta en la posición indicada o null.
     *
     * @param x
     * @param y
     * @return
     */
    protected tPieza getPieza(int x, int y) {
        return this.piezas[x][y];
    }
}
