/** José Sánchez Moreno. 02/02/16 */
package Piezas;

import Piezas.Pieza.tPieza;
import java.awt.Point;
import java.util.List;

/**
 *
 * @author tegus
 */
public class TableroAmenaces extends Tablero
{
    private final boolean[][] amenaçades;
    
    /**
     * Constr.
     * 
     * @param dimension 
     */
    public TableroAmenaces(int dimension)
    {
        super(dimension);
        amenaçades = new boolean[dimension][dimension];
    }
    
    /**
     * Clone.
     * 
     * @param t
     */
    public TableroAmenaces(TableroAmenaces t)
    {
        super(t);
        
        final int dimension = t.getDimension();
        this.amenaçades = new boolean[dimension][dimension];
        
        for(int i = 0; i < dimension; i++)
            for(int j = 0; j < dimension; j++) 
                amenaçades[i][j] = t.amenaçades[i][j]; 
    }
    

    /**
     * Busca el primer punto no vacío y lo devuelve, si no lo hay null.
     * 
     * @return 
     */
    public Point getFirstEmpty()
    {
        Point p;
        final int dimension = getDimension();
        for(int i = 0; i < dimension; i++)
            for (int j = 0; j < dimension; j++) {
                if(!hayPieza( p = new Point(i, j)) && !isAmenaçada(p))
                    return p;
            }
        
        return null;
    }
    
    /**
     * bug amb j
     * nomes es gety sa 1 volta.
     * 
     * @param q
     * @return 
     */
    public Point getNextEmpty(Point q)
    {
        Point p;
        int firstI = (int) q.getX();
        int firstJ = (int) q.getY()+1;
        final int dimension = getDimension();
        for(int i = firstI; i < dimension; i++)
            for (int j = (i == firstI) ? firstJ : 0; j < dimension; j++) {
                if(!hayPieza( p = new Point(i, j)) && !isAmenaçada(p))
                    return p;
            }
        
        return null;
    }
    
    /**
     * Get
     * @param it
     * @return 
     */
    public Point getRowEmpty(taIterador it)
    {
        Point p;
        while( it.is_valid() )
        {
            p = it.get();
            it.Next();
            if( hayPieza(p) || !isAmenaçada(p)) return p;
        }
        return null;
    }

    public TableroAmenaces addPiezaKilling(tPieza tpieza, Point pos)
    {
        //Fa una còpia del tauler
        TableroAmenaces amenazadas = new TableroAmenaces(this);
        //afegeix sa peça a sa posició
        amenazadas.addPieza(tpieza, pos);
        
        //retorna les posicions plenes que pot matar
        //no la llista de punts a la que es capaç de matar
        final Pieza p = PiezaAbstracta.getPieza(tpieza);
        List<Point> lAmenazas = p.amenazar(pos, amenazadas);
        
        //afegeix el booleà a les posicions
        for(Point pos0 : lAmenazas)
            amenazadas.addAmenaça(pos0);
        
        return amenazadas;
    }

    
    public boolean isAmenaçada(Point p)
    {
        if( !dentroTablero(p) ) 
        {
            System.out.println("La posición "+ p +" no está en el tablero");
            return false;
        }

        int x = (int) p.getX();
        int y = (int) p.getY();
        return this.amenaçades[y][x];
    }
    
    private boolean addAmenaça(Point p)
    {
        if( !dentroTablero(p) ) return false;
        
        int x = (int) p.getX();
        int y = (int) p.getY();
        amenaçades[y][x] = true;
        return true;

    }
    
    //Iterador------------------------------------------------------------------
    public class taIterador
    {
        private int counter;
        private final Point ini;
        
        public taIterador( Point p )
        {
            ini = p;
            counter = 0;
        }
        
        public void First() { counter = 0; }
        
        public void Next() { counter = counter + 1; }
        
        public Point get(int n)
        {
            Point p = new Point(
                (int)  ini.getX(),
                (int) (ini.getY()+n) % getDimension()
            );
            return p;
        }
        
        public Point get()
        {
            Point p = new Point(
                (int)  ini.getX(),
                (int) (ini.getY()+counter) % getDimension()
            );
            return p;
        }
        
        public boolean is_valid() { return counter < getDimension()+1; }
    }

}
