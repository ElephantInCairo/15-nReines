/**
 * PROBLEMA (23/12):Averiguar i restringir el ámbito de actuación de Direcciones
 * 
 * PROBLEMA: al hacer inversa sobre un enumerado, cambia el valor en todos
 *  los sitios en los que aparece el enumerado. (SOLUCIONADO)
 * 
 */
package Piezas;

import java.awt.Point;
import java.util.List;

/**
 *
 * @author tegus
 */
public interface Pieza {
    
    enum tPieza
    {
        REINA("Q"), TORRE("T"), ALFIL("A"),
        CAVALL("C"), REY("R"), REY2("K"),
        PEO("P");
        
        private final String s;
        
        tPieza(String str) { s = str; }
        
        public String representacio() { return s; }
    }
    
    enum Direccion
    {
        N  (-1, 0), S  ( 1, 0),
        E  ( 0, 1), O  ( 0,-1),
        NE (-1, 1), NO (-1,-1),
        SE ( 1, 1), SO ( 1,-1);
        
        private final int dx;
        private final int dy;

        Direccion(int a, int b)
        {
            dx = a;
            dy = b;
        }
        
        public Point avança(Point p)
        {
            p.translate(dx, dy);
            return p;
        }
    }
    
    boolean esBlanca();
    
    public boolean esMismoColor(Pieza p);
    
    List amenazar(Point p, TableroAmenaces t);
    
    List matar(Point p, TableroAmenaces t);
    
    List mover(Point p, TableroAmenaces t);
}
