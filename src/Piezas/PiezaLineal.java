package Piezas;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tegus
 */
abstract public class PiezaLineal extends PiezaAbstracta {


    public PiezaLineal(boolean color) {
        super(color);
    }
    
    @Override
    public List amenazar(Point p, TableroAmenaces t) {
        
        List<Direccion> direcciones = getDirecciones();
        List<Point> amenazar = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direcciones) {
            //se mueve de su posicion inicial
            d.avança(p);
            while( t.dentroTablero(p) && !t.hayPieza(p) )
            {
                amenazar.add( new Point(p) );
                d.avança(p);
            }
            //if( t.dentroTablero(p) && !esMismoColor( t.getPieza(p) ) ) amenazar.add( new Point(p) );
            if( t.dentroTablero(p) )
            { 
                amenazar.add( new Point(p) );
                d.avança(p);
            }
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        
        return amenazar;
    }
    

    @Override
    public List matar(Point p, TableroAmenaces t) {
        
        List<Direccion> direcciones = getDirecciones();
        List<Point> matar = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direcciones)
        {
            //se mueve de su posicion inicial
            d.avança(p);
            //mientras la casilla esté dentro, sige
            while( t.dentroTablero(p) )
            {
                //si esta vacía, avanza
                if( !t.hayPieza(p) )
                    d.avança(p);
                //sino (esta llena)
                else
                {
                    //si es de diferente color, la añade
                    //if( !esMismoColor( t.getPieza(p) ) ) 
                        matar.add(new Point(p));
                    //y sale
                    break;
                }
            }
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        
        return matar;
    }
    
    
    @Override
    public List mover(Point p, TableroAmenaces t) {
        
        List<Direccion> direcciones = getDirecciones();
        List<Point> mover = new ArrayList<>();
        Point inicio = new Point(p);
        
        for(Direccion d : direcciones) {
            //se mueve de su posicion inicial
            d.avança(p);
            //mientras la casilla sea valida Y esté vacia, sigue
            while( t.dentroTablero(p) && !t.hayPieza(p) ) {
                //System.out.println("Dir: "+ d +", pos: "+ p);
                mover.add(new Point(p)); 
                d.avança(p);
            }
            //la vuelve a colocar a sitio para que se mueva en otra direccion
            p.setLocation(inicio);
        }
        
        return mover;
    }
    
    abstract public List getDirecciones();
}